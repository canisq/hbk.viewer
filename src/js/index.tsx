import * as React from "react";
import * as ReactDOM from "react-dom";
import { Viewer, IViewerProps } from "./components/Viewer";

import "../scss/main.scss";

var inspirationList: any[] = [].slice.call(document.querySelectorAll(".js-inspirations [data-model]"))
    .map((item: HTMLElement, index: number) => {
        return {
            element: item,
            index: index,
            model: JSON.parse(item.dataset.model)
        };
    });

if (inspirationList.length > 0) {
    let  container = document.createElement("div");
    document.body.appendChild(container);

    ReactDOM.render(<Viewer items={inspirationList} visible={false}  />, container);
}

