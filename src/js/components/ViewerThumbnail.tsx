import * as React from "react";
import { Like } from "./Like";

export interface IViewerThumbnail {
    model: IVieverItemProps
}

export interface IVieverItemProps {
    id: number,
    name: string,
    likes: number,
    authorImage: string,
    authorUrl: string,
    url: string,
    image: string,
    thumb: string,
    videoUrl?: string
}

const ViewerThumbnail: React.StatelessComponent<IViewerThumbnail> = (props) => {
    const style = {
        backgroundImage: `url(${props.model.thumb})`
    };

    const styleImage = {
        backgroundImage: `url(${props.model.image})`
    };

    const like = {
        type: 1,
        id: 424194,
        count: 3,
        state: false,
        source: 4
    }

    const captions = (
        <div className="viewer-thumbnail__caption">
            <div className="media">
                <div className="media-left">
                    <a href={props.model.url}>
                        <img className="media-object" src={props.model.authorImage} width="50" height="50" />
                    </a>
                </div>
                <div className="media-body">
                    <div className="media-heading">
                        <a href={props.model.authorUrl}>{props.model.name}</a>
                    </div>
                </div>
                <div className="media-right text-center">
                    <Like {...like}  />
                </div>
            </div>
        </div>
    );

    const video = () => {
        return props.model.videoUrl ? (<div />) : null;
    }

    return (
        <div className="viewer-thumbnail" style={style}>
            <div className="viewer-thumbnail__image" style={styleImage}>

                {video}

            </div>
        </div>
    );
}

ViewerThumbnail.displayName = "ViewerThumbnail";

export { ViewerThumbnail };