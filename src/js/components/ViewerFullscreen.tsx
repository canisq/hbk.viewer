import * as React from 'react';

export interface IViewerFullscreenProps {
    url: string
};

export const ViewerFullscreen: React.StatelessComponent<IViewerFullscreenProps> = (props) => {
    const style = {
        display: "block"
    };

    const showFullscreen = (event: any): void => {
        const element = event.currentTarget;
        element.parentNode.webkitRequestFullScreen();
        //element.webkitRequestFullScreen();
    };

    return (
        <div>
            <div id="dupa">
                <img src={props.url}  style={style}/>
            </div>
        </div>
    );
}
