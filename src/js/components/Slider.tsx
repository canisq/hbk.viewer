import * as React from 'react';
import * as ReactDOM from "react-dom";
import * as Hammer from "hammerjs";

const HAMMER_OPTIONS = {
    direction: Hammer.DIRECTION_HORIZONTAL,
    threshold: 0,
    pointers: 0
};

export interface ISliderProps extends React.Props<any> {

    current?: number;
    sensitivity?: number,
    speed?: number,
    update?: (state: any) => void
};

export interface ISliderState {
    translateX: number,
    animate: boolean,
    itemsCount: number,
    current: number
};

export class Slider extends React.Component<ISliderProps, ISliderState> {
    contentElement: HTMLElement;

    static defaultProps: Partial<ISliderProps> = {
        current: 0,
        sensitivity: 25,
        speed: 250,
        update: () => { }
    }

    constructor(props: ISliderProps) {
        super(props);
        const children = this.props.children as any;

        this.state = {
            itemsCount: children.length,
            current: this.props.current,
            animate: false,
            translateX: 0
        };
    }

    componentDidMount(): void {
        this.addEvents(this.contentElement);
        this.setCurrent(this.state.current);
    }

    componentWillUpdate(nextProps: ISliderProps, nextState: ISliderState): void {
        if (nextProps !== this.props) {
            const children = nextProps.children as any;
            this.setState({
                itemsCount: children.length,
                current: nextProps.current
            });
            this.setCurrent(nextProps.current);
        }
    }

    addEvents(element: HTMLElement): void {
        const hammer = new Hammer(element);
        hammer.get("pan").set(HAMMER_OPTIONS);
        hammer.on("pan", this.handleSwipe.bind(this));
    }

    handleTransitionEnd(): void {
        this.props.update(this.state);
        this.setState({ animate: false });
    }

    setTranslateX(value: number, animate: boolean = false): void {
        this.setState({
            translateX: value,
            animate: animate
        });
    }

    setCurrent(index: number, animate: boolean = false): void {
        const count = this.state.itemsCount;
        const last = count - 1;
        const current = index < 0 ? 0 : index > last ? last : index;
        const percentage = -(100 / count) * current;

        this.setState({ current: current });
        this.setTranslateX(percentage, animate);
    }

    handleSwipe(event: HammerInput): boolean {
        if (this.state.animate) {
            return false;
        }

        const current = this.state.current;
        const percentage = 100 / this.state.itemsCount * event.deltaX / window.innerWidth;
        const transform = percentage - 100 / this.state.itemsCount * current;
        const sensitivity = this.props.sensitivity / this.state.itemsCount;
        let nextCurrent = current;

        this.setState({ translateX: transform });

        if (event.isFinal) {
            if (event.velocityX > 1) {
                nextCurrent = current - 1;
            } else if (event.velocityX < -1) {
                nextCurrent = current + 1;
            } else {
                if (percentage >= sensitivity) {
                    nextCurrent = current - 1;
                } else if (percentage <= -sensitivity) {
                    nextCurrent = current + 1;
                }
            }

            this.setCurrent(nextCurrent, true);
        }

        return true;
    }

    render(): JSX.Element {
        const styleContent = {
            width: `${100 * this.state.itemsCount}vw`,
            transform: `translateX(${this.state.translateX}%)`,
            willChange: "transform",
            transition: this.state.animate ? `transform ${this.props.speed}ms ease-in-out` : ""
        }

        return (
            <div className="hbk-slider">
                <div className="hbk-slider__content"
                    style={styleContent}
                    ref={(ref) => this.contentElement = ref}
                    onTransitionEnd={this.handleTransitionEnd.bind(this)}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
