import * as React from "react";

import { Modal, IModalProps, ModalAnimationType } from "./Modal";
import { Slider, ISliderProps } from "./Slider";
import { ViewerThumbnail, IVieverItemProps } from "./ViewerThumbnail";
import { ViewerFullscreen, IViewerFullscreenProps } from "./ViewerFullscreen";

const enum Orientation {
    PORTRAIT,
    LANDSCAPE
}

const enum Key {
    ARROW_LEFT = 37,
    ARROW_UP = 38,
    ARROW_RIGHT = 39,
    ARROW_DOWN = 40,
    ESC = 27
}

interface IViewerItem {
    element: HTMLElement,
    index: number,
    model: IVieverItemProps
}

export interface IViewerState {
    visible: boolean,
    current: number,
    items: any[],
}

export interface IViewerProps {
    items: IViewerItem[],
    visible?: boolean,
    current?: number,
    range?: number
}

export class Viewer extends React.Component<IViewerProps, IViewerState> {

    static defaultProps: Partial<IViewerProps> = {
        visible: false,
        current: 0,
        range: 3
    }

    constructor(props: IViewerProps) {
        super(props);
        this.state = {
            items: this.props.items,
            visible: this.props.visible,
            current: this.props.current
        }
    }

    componentDidMount(): void {
        this.addEventsToDomItems(this.state.items);
    }

    componentDidUpdate(prevProps: IViewerProps, prevState: IViewerState): void {
        if(!this.state.visible) {
            const element: HTMLElement = this.state.items[this.state.current].element;
            window.scrollBy(0, element.getBoundingClientRect().top);
        }
    }

    addEventsToDomItems(items: any[]): void {
        items.forEach((item): void => {
            item.element.addEventListener('click', (event: Event): void => {
                this.setState({ current: item.index, visible: true });
                event.preventDefault();
            }, false)
        })
    }

    next(): void {
        this.setState((prev) => {
            var current = prev.current + 1;
            var last = prev.items.length - 1;

            return {
                current: current < last ? current : 0
            };
        })
    }

    prev(): void {
        this.setState((prev) => {
            var current = prev.current - 1;
            return {
                current: current < 0 ? 0 : current
            };
        })
    }

    hide(): void {
        this.setState({ visible: false })
    }

    getItems(): IViewerItem[] {
        const c = this.state.current;
        const m = this.state.items.length;
        const r = this.props.range;
        const s = c - 1 < 0 ? 0 : c - 1;
        const e = s + r;

        return this.state.items.slice(s, e);
    }

    showFullscreen() {

    }

    render(): JSX.Element {
        const current = this.state.current;
        const items = this.state.items;
        const currentItemModel = items[current].model;

        const modelProps: IModalProps = {
            visible: this.state.visible,
            hideModal: this.hide.bind(this),
            showClose: true,
            animationType: ModalAnimationType.NONE
        };

        const sliderProps: ISliderProps = {
            current: current === 0 ? 0 : 1,
            update: (state: any) => {
                if (sliderProps.current < state.current) {
                    this.next();
                } else if (sliderProps.current > state.current) {
                    this.prev();
                }
            }
        };

        const slides = this.getItems().map((item, idx) => {
            return (
                <ViewerThumbnail model={item.model} key={idx} />
            );
        });

        const FullscreenButton = (props: any) => {
            return (
                <button className="viewer-fullscreen">
                    a
                    <svg viewBox="0 0 24 24" width="30" height="30">
                        <path fill="currentColor" d="M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z" />
                    </svg>
                </button>
            );
        }

        //window.history.pushState(null, null, items[current].model.url);

        return (
            <div className="hbk-viewer">
                <Modal {...modelProps} >
                    <FullscreenButton />
                    <Slider {...sliderProps}>{slides}</Slider>
                </Modal>
            </div>
        );
    }
}