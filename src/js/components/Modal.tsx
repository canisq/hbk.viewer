import * as React from "react";

const BODY_ELEMENT: HTMLElement = document.body;

const CLASS_NAMES = {
    base: "hbk-modal",
    content: "hbk-modal__content",
    body: "hbk-modal__body",
    header: "hbk-modal__header",
    close: "hbk-modal__close",
    hasModal: "has-modal"
};

export enum ModalAnimationType {
    NONE = 0,
    SLIDE_LEFT = 1,
    SLIDE_TOP = 2,
    SLIDE_RIGHT = 3,
    SLIDE_BOTTOM = 4
};

export interface IModalProps extends React.Props<any> {
    title?: string,
    visible: boolean,
    showClose: boolean,
    animationType: ModalAnimationType,
    hideModal: () => void
}

export const Modal: React.StatelessComponent<IModalProps> = (props) => {
    const style = {
        transform: `translateX(${props.visible ? 0: 100 }vw)`,
        transition: 'transform 250ms ease-in-out'
    };

    const closeButton = props.showClose ? (
        <button className={CLASS_NAMES.close} onClick={props.hideModal}>
            <svg viewBox="0 0 24 24" width="30" height="30">
                <path fill="currentColor" d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z" />
            </svg>
        </button>
    ) : null;

    const title = props.title ? (
        <h3 className={CLASS_NAMES.header}>
            {props.title}
        </h3>
    ) : null;

    BODY_ELEMENT.classList.toggle(CLASS_NAMES.hasModal, props.visible);

    return (
        <div className={`${CLASS_NAMES.base}`} style={style}>
            <div className={CLASS_NAMES.content}>
                {title}
                {closeButton}
                <div className={CLASS_NAMES.body}
                    tabIndex={-1}
                    onKeyUp={this.handleKeyUp}>
                    {props.children}
                </div>
            </div>
        </div>
    );
}

Modal.displayName = "Modal";
