import * as React from "react";

export interface ILikeProps {
    type: number,
    id: number,
    count: number,
    state: boolean,
    source: number
};

export const Like = (props: ILikeProps) => {
    return (
        <button type="button" className="btn btn-like viewer-thumbnail__like js-like-button"
            data-itemtype={props.type}
            data-itemid={props.id}
            data-count={props.count}
            data-state={props.state}
            data-source={props.source}>
            <i className="fa fa-heart-o fa-lg" aria-hidden="true"></i>
        </button>
    );
}